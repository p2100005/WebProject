<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>500 Error - Server Error</title>
    <!-- Bootstrap CSS -->
    <!-- Custom CSS -->
    <style>
        body {
            background-color: #f8f9fa;
        }
        .container {
            margin-top: 50px;
        }
        h1 {
            font-size: 72px;
            margin-bottom: 0;
        }
        p {
            font-size: 24px;
        }
        img {
            display: block;
            margin: auto;
            margin-top: 50px;
            width: 200px;
        }
    </style>
</head>
<body>
<div class="container text-center">
    <h1>500</h1>
    <p class="lead"> Oups! Un problème s'est produit de notre côté. Veuillez réessayer plus tard.</p>
    <img src="https://t.ly/5YRZ" alt="Pikachu crying">
</div>
