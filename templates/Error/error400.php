<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">

    <title>400 Bad Request</title>
</head>
<body>
<div class="container mb-3">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card border-danger">
                <div class="card-header bg-danger text-white">
                    <h4 class="mb-9">Erreur 400</h4>
                </div>
                <div class="card-body">
                    <img src="https://media.tenor.com/ct6giWED9-4AAAAC/puplip-pokemon.gif" class="img-fluid" alt="400 Bad Request">
                    <p class="card-text">Oh non , tu as rencontré une erreur 404</p>
                    <?= $this->Html->link('Go back', 'javascript:history.back()', ['class' => 'btn btn-danger']) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
