<section id="main-content">
    <section class="wrapper" >
        <div class="row mt">
            <div class="col-md-12">
                <section class="task-panel tasks-widget">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h5><i class="fa fa-tasks"></i> Ajouter une carte</h5>
                        </div>
                        <br>
                    </div>
                    <div class="panel-body">
                        <div class="task-content">
                            <?php echo $this->Form->create(null, ['type' => 'post', 'class' => "form-horizontal style-form"]); ?>
                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label" for="inputTitle">Nom du pokemon</label>
                                <div class="col-sm-10">
                                    <?php
                                    echo $this->Form->control('NomCarte', [
                                        'label' => false,
                                        "class" => "form-control",
                                        "name" => "Nomcarte",
                                        "id" => "inputNomCarte"
                                    ]);
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label" for="inputPV">Points de vie</label>
                                <div class="col-sm-10">
                                    <?php
                                    echo $this->Form->control('PV', [
                                        'label' => false,
                                        "class" => "form-control",
                                        "name" => "PV",
                                        "id" => "inputPV"
                                    ]);
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label" for="inputType">Type du pokemon</label>
                                <div class="col-sm-10">
                                    <?php
                                    $levels = [
                                        "Feu" => "Feu",
                                        "Eau" => "Eau",
                                        "Plante" => "Plante",
                                        "Electricité" => "Electricité",
                                        "Psy" => "Psy",
                                        "Combat" => "Combat",
                                        "Tenebres.php" => "Tenebres.php",
                                        "Metal" => "Metal",
                                        "Fee" => "Fee"

                                    ];








                                    echo $this->Form->select('Type', $levels, [
                                        'default' => '0',
                                        'id' => 'inputType',
                                        'class' => 'form-control',
                                        'name' => 'Type',
                                        'label' => false,
                                    ]);
                                    ?>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 col-sm-2 control-label" for="inputPrix">Prix</label>
                                    <div class="col-sm-10">
                                        <?php
                                        echo $this->Form->control('Prix', [
                                            'label' => false,
                                            "class" => "form-control",
                                            "name" => "Prix",
                                            "id" => "inputPrix"
                                        ]);
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 col-sm-2 control-label" for="inputImage">Lien vers l'image</label>
                                    <div class="col-sm-10">
                                        <?php
                                        echo $this->Form->control('Image', [
                                            'label' => false,
                                            "class" => "form-control",
                                            "name" => "Image",
                                            "id" => "inputImage"
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <?php
                                echo $this->Form->button('Ajouter', ['type' => 'submit', 'class' => 'btn btn-primary']);
                                ?>
                            </div>
                            <?= $this->Form->end(); ?>

                            <?= $this->Html->link('Export Json', ['controller' => 'Carts', 'action' => 'jsonexport'], ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
