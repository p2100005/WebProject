<header class="header">
    <nav class="navbar navbar-expand-lg bg-primary mb-3">
        <a class="navbar-brand" href="#"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/International_Pok%C3%A9mon_logo.svg/2560px-International_Pok%C3%A9mon_logo.svg.png" alt="Mon logo" class="w-25 ms-2 px-2 py-2"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <div class="navbar-nav ml-auto">
                <?php
                    echo $this->Html->image("user.png", [
                                            "alt" => "Brownies",
                                            'url' => ['controller' => 'Users', 'action' => 'Login', 6],
                                            'class' => 'btn bg-primary w-25'
                                            ]);
                ?>
                <?php
                    echo $this->Html->image("shopping-cart.png", [
                                            "alt" => "Brownies",
                                            'url' => ['controller' => 'Panier', 'action' => 'index', 6],
                                            'class' => 'btn bg-primary w-25'
                                            ]);
                ?>
                <?php
                    echo $this->Html->image("add.png", [
                                            "alt" => "Brownies",
                                            'url' => ['controller' => 'Carts', 'action' => 'add', 6],
                                            'class' => 'btn bg-primary w-25'
                                            ]);
                ?>
            </div>
        </div>
    </nav>
</header>
