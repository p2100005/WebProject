<img src="https://cdn.pixabay.com/photo/2016/07/13/08/31/pokemon-1513925_960_720.jpg" alt="pokeball" width="15%"/>

# Pokemon Card Website
Website to sell your pokemon cards with others.

## Table of Contents
1. [Technologies](#technologies)
2. [Installation](#installation)
3. [Update](#update)
4. [Configuration](#configuration)
5. [Layout](#layout)

## Technologies

A list of technologies used within the project:
* [PHP](https://www.php.net/): Version 7.4
* [CakePHP](https://cakephp.org): Version 4.4
* [Bootstrap](https://getbootstrap.com): Version 4.0.0
* [MySQL](https://www.mysql.com): Version 8.0.32

## Installation

1. Install [PHP](https://www.php.net/downloads) -> Min. Version: 7.4
2. Clone or Download the project.
3. Inside the project, download [Composer](https://getcomposer.org/doc/00-intro.md) or update him with `composer update` in the terminal. Then run `composer install`
4. Install Bootstrap with Composer. Run `composer require twbs/bootstrap:4.0.0`.

You can now either use your machine's webserver to view the default home page, or start
up the built-in webserver with:

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765` to see the welcome page.

## Update

Since this skeleton is a starting point for your application and various files
would have been modified as per your needs, there isn't a way to provide
automated upgrades, so you have to do any updates manually.

## Configuration

Read and edit the environment specific `config/app_local.php` and setup the
`'Datasources'` and any other configuration relevant for your application.
Other environment agnostic settings can be changed in `config/app.php`.
By default, we use a MySQL server.

## Layout

The app skeleton uses [Milligram](https://milligram.io/) (v1.3) minimalist CSS
framework by default and [Bootstrap](https://getbootstrap.com/) (v4.0.0) with SASS. You can, however, replace it with any other library or
custom styles.

***

> "In Pokemon battles... It's not just about deciding a winner, it's about your team! You! Your opponent! And his team! A subtle equation with 4 unknowns" - Natural Harmonia Gropius, N -

<img src="https://cdn.pixabay.com/photo/2023/03/10/22/21/pikachu-7843241_960_720.png" alt="pikachu" width="15%"/>