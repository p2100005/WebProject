<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Event\EventInterface;

class CartsController extends AppController
{
    public function index()
    {
    }

    public function add()
    {

        $pokemon = $this->Carts->newEmptyEntity();
        if ($this->request->is('post')) {
            $pokemon = $this->Carts->patchEntity($pokemon, $this->request->getData());
            echo $pokemon;
            if ($this->Carts->save($pokemon)) {
                $this->Flash->success(__('The pokemon has been saved.'));



               return  $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('The pokemon could not be saved. Please, try again.'));
        }
    }

    public function remove()
    {
    }

    public function jsonexport()
    {
        $this->autoRender = false;
        $this->response = $this->response->withType('json');
        $this->response->getBody()->write(json_encode($this->Carts->find('all')->toArray()));;
        $this->response = $this->response->withDownload('Datas.json');
        return $this->response;

    }

    public function panieradd($idCarte){



        $cart = $this->request->getSession()->read('cart')??[];
        $cart[] = $idCarte;
        $this->request->getSession()->write('cart', $cart);
        $this->Flash->success(__('The pokemon has been saved.'));
        $this->redirect($this->referer());




    }

    public function feu()
    {

        $carts = $this->Carts->find('all')->where(['Type' => 'Feu']);
        $this->set(compact('carts'));

    }

    public function eau()
    {

            $carts = $this->Carts->find('all')->where(['Type' => 'Eau']);
            $this->set(compact('carts'));

    }

    public function plante()
    {
        $carts = $this->Carts->find('all')->where(['Type' => 'Plante']);
        $this->set(compact('carts'));


    }
    public function electrique()
    {
        $carts = $this->Carts->find('all')->where(['Type' => 'Electricité']);
        $this->set(compact('carts'));

    }
    public function psy()
    {
        $carts = $this->Carts->find('all')->where(['Type' => 'Psy']);
        $this->set(compact('carts'));

    }



    public function combat()
    {
        $carts = $this->Carts->find('all')->where(['Type' => 'Combat']);
        $this->set(compact('carts'));


    }

    public function tenebres ()
    {
        $carts = $this->Carts->find('all')->where(['Type' => 'Tenebres']);
        $this->set(compact('carts'));

    }

    public function acier(){
        $carts = $this->Carts->find('all')->where(['Type' => 'Metal']);
        $this->set(compact('carts'));

    }

    public function fee(){
        $carts = $this->Carts->find('all')->where(['Type' => 'Fee']);
        $this->set(compact('carts'));


    }




}
